import { Component } from '@angular/core';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { Stepcounter } from '@ionic-native/stepcounter/ngx';
import { Platform } from '@ionic/angular';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  hide = true;
  steps = 7889;
  kilometer: any;
  progress : any;

  users: any[] = [
    {
      id: 1,
      first: 'Alice',
      last: 'Smith',
    },
    {
      id: 2,
      first: 'Bob',
      last: 'Davis',
    },
    {
      id: 3,
      first: 'Charlie',
      last: 'Rosenburg',
    }
  ];

  patient = {
    name : 'Mukunda',
    age : '24 yrs',
    oxy : '98.2',
    time : '7:00 PM',
    temp : '36.7',
    heart_rate : '67',
    description : 'Your legs are made up of bones, blood vessels, muscles, and other connective tissue. ',
    hide_data : 'They are important for motion and standing.'
  }

  constructor(
    public stepcounter: Stepcounter,
    public platform: Platform,
    public localNotifications: LocalNotifications
  ) {
    this.kilometer = this.steps / 1312.33595801;
    this.kilometer = this.kilometer.toFixed(2);
    this.progress = this.steps / 10000
    console.log(this.kilometer);

  }
}
